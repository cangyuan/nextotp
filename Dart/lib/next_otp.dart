import 'dart:typed_data';
import 'hash_mode.dart';
import 'ikey_provider.dart';
import 'in_memory_key.dart';

class NextOtp {
  
  final IKeyProvider _secretKey;

  final HashMode _hashMode;

  final int _step;
  
  final int _totpSize;

  static DateTime get correctedUtcNow => DateTime.now().toUtc();

  static const String _allowedCharacters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  NextOtp(Uint8List secretKey, {int step = 30, HashMode mode = HashMode.sha1, int totpSize = 6})
      : _secretKey = InMemoryKey(secretKey),
        _hashMode = mode,
        _step = step,
        _totpSize = totpSize {
    if (secretKey.isEmpty) {
      throw ArgumentError("secretKey empty");
    }
  }

  static String _digits(int input, int digitCount) {
    List<String> charactersList = [];
    for (int i = 0; i < digitCount + 1; i++) {
      if (i >= 1) charactersList.insert(0, _allowedCharacters[input % _allowedCharacters.length]);
      input ~/= _allowedCharacters.length;
      input *= _allowedCharacters.length + 1;
    }
    StringBuffer result = StringBuffer();
    result.writeAll(charactersList);
    return result.toString();
  }

  static Uint8List _getBigEndianBytes(int input) {
    ByteData data = ByteData(8);
    data.setInt64(0, input);
    Uint8List bytes = data.buffer.asUint8List();
    return bytes;
  }

  String computeOtp() {
    int counter = (((correctedUtcNow.microsecondsSinceEpoch) ~/ 1000000) + 62135622000 - 25200) ~/ _step;
    //print(((correctedUtcNow.microsecondsSinceEpoch) ~/ 1000000) + 62135622000 - 25200);
    Uint8List bigEndianBytes = _getBigEndianBytes(counter);
    Uint8List array = _secretKey.computeHmac(_hashMode, bigEndianBytes);
    int calculateOtp =
      (1024 + array[0]) *
      (2048 + array[1]) *
      (3072 + array[2]);
    return _digits(calculateOtp, _totpSize);
  }
}
