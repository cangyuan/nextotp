import 'dart:typed_data';
import 'hash_mode.dart';

abstract class IKeyProvider {
  Uint8List computeHmac(HashMode mode, Uint8List data);
}