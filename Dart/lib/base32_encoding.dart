import 'dart:typed_data';

class Base32Encoding {
  static Uint8List toBytes(String input) {
    if (input.isEmpty) {
      throw ArgumentError('input');
    }

    input = input.replaceAll(RegExp('=+\$'), '');
    int num = input.length * 5 ~/ 8;
    Uint8List array = Uint8List(num);
    int b = 0;
    int b2 = 8;
    int num2 = 0;
    String text = input;
    for (int i = 0; i < text.length; i++) {
      int num3 = _charToValue(text[i]);
      if (b2 > 5) {
        int num4 = num3 << b2 - 5;
        b = b | num4;
        b2 -= 5;
      } else {
        int num4 = num3 >> 5 - b2;
        b = b | num4;
        array[num2++] = b;
        b = num3 << 3 + b2;
        b2 += 3;
      }
    }

    if (num2 != num) {
      array[num2] = b;
    }

    return array;
  }

  static int _charToValue(String c) {
    if (c.compareTo('@') > 0 && c.compareTo('[') < 0) {
      return c.codeUnitAt(0) - 65;
    }

    if (c.compareTo('1') > 0 && c.compareTo('8') < 0) {
      return c.codeUnitAt(0) - 24;
    }

    if (c.compareTo('`') > 0 && c.compareTo('{') < 0) {
      return c.codeUnitAt(0) - 97;
    }

    throw ArgumentError('c');
  }
}
