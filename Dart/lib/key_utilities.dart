import 'dart:typed_data';
import 'dart:math';

class KeyUtilities {
  static void destroy(Uint8List sensitiveData) {
    ArgumentError.checkNotNull(sensitiveData);
    final random = Random.secure();
    for (var i = 0; i < sensitiveData.length; i++) {
      sensitiveData[i] = random.nextInt(256);
    }
  }
}
