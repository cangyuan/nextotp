import 'dart:typed_data';
import 'hash_mode.dart';
import 'ikey_provider.dart';
import 'key_utilities.dart';
import 'package:crypto/crypto.dart' as crypto;

class InMemoryKey implements IKeyProvider {
  final List<int> _keyData;

  InMemoryKey(List<int> key)
      : _keyData = List<int>.from(key) {
    if (_keyData.isEmpty) {
      throw ArgumentError('The key must not be empty');
    }
  }

  static crypto.Hmac _createHmacHash(HashMode otpHashMode, Uint8List keyData) {
    switch (otpHashMode) {
      case HashMode.sha256:
        return crypto.Hmac(crypto.sha256, keyData);
      case HashMode.sha512:
        return crypto.Hmac(crypto.sha512, keyData);
      default:
        return crypto.Hmac(crypto.sha1, keyData);
    }
  }

  @override
  Uint8List computeHmac(HashMode mode, Uint8List data) {
    final hmac = _createHmacHash(mode, Uint8List.fromList(_keyData));
    try {
      return Uint8List.fromList(hmac.convert(data).bytes);
    } finally {
      KeyUtilities.destroy(Uint8List.fromList(_keyData));
    }
  }
}
