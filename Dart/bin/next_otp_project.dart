import 'dart:typed_data';
import 'package:next_otp_project/base32_encoding.dart';
import 'package:next_otp_project/hash_mode.dart';
import 'package:next_otp_project/next_otp.dart';

Future<void> main(List<String> arguments) async {
  Uint8List byteX = Base32Encoding.toBytes("W7MARRQG5A2QRWMY56JG3EV3ACB7FLIU");
  do{    
    String otpCode = NextOtp(byteX, step: 5, mode: HashMode.sha1, totpSize: 12).computeOtp();
    print(otpCode);
    await Future.delayed(const Duration(seconds: 5));
  } while(true);  
}
